# Herd Immunity

Programs for computing herd immunity levels.

An R version (interpretation?) of Britton et als. (2020) code in https://github.com/tombritton65/Herd-immunity.
